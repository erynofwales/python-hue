# test_light.py
# Eryn Wells <eryn@erynwells.me

import json
import unittest

from hue.light import Light, State
from hue.http import Bridge

DATA = json.loads('''{
    "state": {
        "hue": 50000,
        "on": true,
        "effect": "none",
        "alert": "none",
        "bri": 200,
        "sat": 200,
        "ct": 500,
        "xy": [0.5, 0.5],
        "reachable": true,
        "colormode": "hs"
    },
    "type": "Living Colors",
    "name": "LC 1",
    "modelid": "LC0015",
    "swversion": "1.0.3",
    "pointsymbol": {
        "1": "none",
        "2": "none",
        "3": "none",
        "4": "none",
        "5": "none",
        "6": "none",
        "7": "none",
        "8": "none"
    }
}''')


class LightTestBridge(Bridge):
    def __init__(self):
        super().__init__('lightbridge', 'eryn')


class LightBridge(LightTestBridge):
    def _do_request(self, method, url, data):
        assert self._method_from_function(method) == 'GET'
        assert url == 'http://lightbridge/api/eryn/lights/1'
        return DATA

class LightStateOnTestBridge(LightTestBridge):
    def _do_request(self, method, url, data):
        assert self._method_from_function(method) == 'PUT'
        assert url == 'http://lightbridge/api/eryn/lights/1/state'
        return [{'success': {'/lights/1/state/on': data['on']}}]


def make_light():
    return Light(LightBridge(), DATA, 'light/1')

class TestLight(unittest.TestCase):
    def setUp(self):
        self.light = make_light()

    def test_light_default_field_values(self):
        assert self.light.type == 'Living Colors'
        assert self.light.name == 'LC 1'
        assert self.light.model == 'LC0015'
        assert self.light.software_version == '1.0.3'
        assert isinstance(self.light.state, State)

    def test_light_state(self):
        assert self.light.state.on == True

def test_state_off():
    light = Light(LightStateOnTestBridge(), DATA, 'lights/1')
    light.state.on = False
    assert light.state.on == False
    light.state.on = True
    assert light.state.on == True
