# hue.py
# Eryn Wells <eryn@erynwells.me>

import enum
import json
import logging
import socket
from os.path import join

from . import http
from .base import Object
from .light import Light


LOG = logging.getLogger('hue')


class Hue:
    '''Top-level object for interacting with a Hue hub.'''

    # Max length of these strings, as defined in the API.
    # http://www.developers.meethue.com/documentation/configuration-api#71_create_user
    APP_NAME_MAX_LENGTH = (0, 20)
    DEVICE_NAME_MAX_LENGTH = (0, 19)
    DEVICETYPE_MAX_LENGTH = (0, 40)

    def __init__(self, host, username=None):
        # TODO: Do auto-discovery?
        self.bridge = http.Bridge(host, username)
        self._lights = None

    def make_devicetype(self, app_name, device_name):
        if app_name and device_name:
            return '{}#{}'.format(app_name, device_name)
        elif app_name:
            return app_name
        elif device_name:
            return device_name
        return None

    @property
    def logged_in(self):
        return self.bridge.username is not None

    @property
    def lights(self):
        if not self.logged_in:
            # TODO: Raise a proper exception indicating that you need to log in first.
            return None

        data = self.bridge.get('lights')
        self._lights = [Light(self.bridge, data[i], join('lights', i)) for i in data.keys()]

        return self._lights

    def connect(self, app_name=None, device_name=None, username=None):
        devicetype = self.make_devicetype(app_name, device_name)
        if not devicetype:
            devicetype = 'hue#{}'.format(socket.gethostname())

        data = {'devicetype': devicetype}
        if username:
            data['username'] = username

        data = self.bridge.post('', data)

        # Response data is a list with a single item.
        if 'success' not in data[0]:
            return False

        self.bridge.username = data[0]['success']['username']
        return True
