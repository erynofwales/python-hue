# http.py
# Eryn Wells <eryn@erynwells.me>

import json
import logging
# TODO: Import Unix path module
import os.path

import requests


LOG = logging.getLogger('hue.http')


class HueError(RuntimeError):
    pass


class HTTPError(HueError):
    def __init__(self, code):
        self.code = code


class APIError(HueError):
    def __init__(self, error):
        self.code = error['type']
        self.address = error['address']
        self.description = error['description']

    def __repr__(self):
        return '<{0.__class__.__name__} code={0.code} address={0.address} description={0.description}>'.format(self)


class Bridge:
    '''
    A wrapper around the requests library to perform HTTP requests against a Hue hub.
    '''
    def __init__(self, host, username=None):
        self.host = host
        self.username = username

    def _api_url(self, path):
        url = 'http://{}/api'.format(self.host)
        if self.username:
            url = os.path.join(url, self.username)
        url = os.path.join(url, path)
        return url

    def _method_from_function(self, f):
        try:
            return {requests.get: 'GET',
                    requests.post: 'POST',
                    requests.put: 'PUT',
                    requests.delete: 'DELETE'}[f]
        except KeyError:
            raise ValueError('Invalid HTTP method: {}'.format(f.__name__))

    def _do_request(self, method, url, data):
        kwargs = {}
        if data:
            kwargs['data'] = json.dumps(data)

        LOG.info('{} {} <-- {}'.format(self._method_from_function(method),
            url, json.dumps(data, sort_keys=True, indent=2)))

        r = method(url, **kwargs)
        rdata = r.json()

        LOG.info('{} {} --> {} {}'.format(self._method_from_function(method),
            url, r.status_code, json.dumps(rdata, sort_keys=True, indent=2)))

        if r.status_code != 200:
            raise HTTPError(r.status_code)
        try:
            if 'error' in rdata[0]:
                raise APIError(rdata[0]['error'])
        except IndexError:
            pass
        except KeyError:
            pass

        return rdata

    def get(self, path, data=None):
        return self._do_request(requests.get, self._api_url(path), data)

    def post(self, path, data=None):
        return self._do_request(requests.post, self._api_url(path), data)

    def put(self, path, data=None):
        return self._do_request(requests.put, self._api_url(path), data)

    def delete(self, path, data=None):
        return self._do_request(requests.delete, self._api_url(path), data)
