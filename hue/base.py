# base.py
# Eryn Wells <eryn@erynwells.me>

import logging
from os.path import join

from .fields import Field, ObjectField

LOG = logging.getLogger('hue')


class Meta(type):
    def __new__(cls, clsname, bases, clsdict):
        for key, field in clsdict.items():
            if isinstance(field, Field):
                if not field.field_name:
                    field.field_name = key
                if not field.api_name:
                    field.api_name = key
        return super().__new__(cls, clsname, bases, clsdict)


class Object(metaclass=Meta):
    def __init__(self, bridge, data, path):
        self._bridge = bridge
        self._data = data
        self._path = path

        # Create an object in _data for ObjectClass fields
        for name, field in vars(self.__class__).items():
            if not isinstance(field, ObjectField):
                continue
            self._data[field.api_name] = field.object_class(self._bridge,
                                                            self._data[field.api_name],
                                                            join(self._path, field.api_name))

        # When the Object is invoked as a context manager, this will be a
        # non-None value. When the context manager exits, all the values set in
        # the context will be updated together.
        self.__context = None

        # Mapping of API paths to Field objects
        self.__paths_to_fields = {}
        for name, field in vars(self.__class__).items():
            if not isinstance(field, Field):
                continue
            self.__paths_to_fields[join('/', self._path, field.api_name)] = field

    def __enter__(self):
        LOG.debug('Beginning batch update on {}'.format(self._path))
        self.__context = {}
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None or exc_value is not None or traceback is not None:
            self.__context = None
            return
        LOG.debug('Batch updating {} properties: {}'.format(self._path, ', '.join(self.__context.keys())))
        data = self._bridge.put(self._path, self.__context)
        self.update_data_values(data)
        self.__context = None

    def update_field_value(self, field, value):
        if self.__context is not None:
            self.__context[field.api_name] = value
        else:
            LOG.debug('Updating {}: {}'.format(field.api_name, value))
            data = self._bridge.put(self._path, {field.api_name: value})
            self.update_data_values(data)

    def update_data_values(self, updates):
        for update in updates:
            if 'error' in update:
                # TODO: Be more graceful
                assert False
            # There should only be one item in the update dictionary.
            for path, value in update['success'].items():
                field = self.__paths_to_fields[path]
                self._data[field.api_name] = value
