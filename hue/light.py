# light.py
# Eryn Wells <eryn@erynwells.me>

import enum

from . import base
from . import fields


class State(base.Object):
    '''Hue light state.'''

    class Alert(enum.Enum):
        Off = 'none'
        Select = 'select'
        LongSelect = 'lselect'

    class Effect(enum.Enum):
        Off = 'none'
        ColorLoop = 'colorloop'

    on = fields.BoolField()
    reachable = fields.BoolField(readonly=True)

    alert = fields.EnumField(Alert)
    effect = fields.EnumField(Effect)

    brightness = fields.ScaledInt8Field('bri', max_value=254)
    hue = fields.ScaledInt16Field()
    saturation = fields.ScaledInt8Field('sat', max_value=254)


class Light(base.Object):
    '''A Hue light.'''

    name = fields.StringField(max_length=32)
    id = fields.StringField('uniqueid', min_length=6, max_length=32, readonly=True)
    model = fields.StringField('modelid', min_length=6, max_length=6, readonly=True)
    software_version = fields.StringField('swversion', min_length=8, max_length=8, readonly=True)
    type = fields.StringField('type', readonly=True)

    state = fields.ObjectField(State)
