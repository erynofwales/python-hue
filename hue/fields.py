# fields.py
# Eryn Wells <eryn@erynwells.me>

import enum
from os.path import join


class Field:
    def __init__(self, api_name=None, field_name=None, *, readonly=False):
        self.api_name = api_name
        self.field_name = field_name
        self.readonly = readonly

    def __get__(self, instance, cls):
        if instance is None:
            return self
        return instance._data[self.api_name]

    def __set__(self, instance, value):
        if self.readonly:
            raise TypeError('{} is readonly'.format(self.field_name))
        instance.update_field_value(self, value)

    def __delete__(self, instance):
        raise AttributeError('Delete not supported')


class Typed(Field):
    field_type = type
    # Subclasses should set valid_types to a tuple of types of which to allow incoming
    # values
    valid_types = None

    def __set__(self, instance, value):
        if not isinstance(value, self.valid_types if self.valid_types else self.field_type):
            raise TypeError('Expected value of type {}, got {} instead'.format(
                            self.field_type.__name__, type(value).__name__))
        super().__set__(instance, self.field_type(value))


class BoolField(Typed):
    field_type = bool


class EnumField(Field):
    def __init__(self, enum_class, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not issubclass(enum_class, enum.Enum):
            raise TypeError('enum_class must be a subclass of enum.Enum')
        self.enum_class = enum_class

    def __get__(self, instance, cls):
        return self.enum_class(super().__get__(instance, cls))

    def __set__(self, instance, value):
        super().__set__(instance, value.value)


class FloatField(Typed):
    field_type = float
    valid_types = (float, int)


class Integer(Typed):
    field_type = int


class RangeBound(Field):
    def __init__(self, *args, max_value=None, **kwargs):
        super().__init__(*args, **kwargs)
        if max_value:
            self.max_value = max_value

    def __set__(self, instance, value):
        if value < 0:
            raise ValueError('Negative values not allowed')
        if value > self.max_value:
            raise ValueError('Integer value too big: {}'.format(value))
        super().__set__(instance, value)


class Scaled(Integer, RangeBound):
    def __get__(self, instance, cls):
        return float(super().__get__(instance, cls)) / self.max_value

    def __set__(self, instance, value):
        super().__set__(instance, self.field_type(self.clamp_value(value) * self.max_value))

    def clamp_value(self, value):
        if value < 0.0:
            return 0.0
        if value > 1.0:
            return 1.0
        return value


class Int8Field(Integer, RangeBound):
    max_value = 255

class ScaledInt8Field(Scaled, Int8Field):
    pass


class Int16Field(Integer, RangeBound):
    max_value = 65535

class ScaledInt16Field(Scaled, Int16Field):
    pass


class ObjectField(Field):
    def __init__(self, object_class, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.object_class = object_class

    def __set__(self, instance, value):
        raise AttributeError('Setting whole objects not supported')


class Sized(Field):
    def __init__(self, *args, min_length=0, max_length=None, **kwargs):
        super().__init__(*args, **kwargs)
        if min_length < 0:
            raise ValueError('Minimum string length must be >= 0')
        if max_length is not None and max_length < min_length:
            raise ValueError('Maximum string length must be >= {}'.format(min_length))
        self.min_length = min_length
        self.max_length = max_length

    def __set__(self, instance, value):
        len_value = len(value)
        if len_value < self.min_length:
            raise ValueError('String value too small')
        if self.max_length and len_value > self.max_length:
            raise ValueError('String value too big')
        super().__set__(instance, value)


class StringField(Typed, Sized):
    field_type = str
