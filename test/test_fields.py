# test_fields.py
# Eryn Wells <eryn@erynwells.me>

import enum
import functools
import json
import unittest
from os.path import join

from nose.tools import nottest, raises

from hue.base import Object
from hue.fields import *


OBJECT_JSON = {
    'api_readonly_field': 'foo',
    'bool_field': True,
    'float_field': 4.6,
    'int8_field': 2,
    'int16_field': 42,
    'string_field': 'abc',
    'short_string_field': 'abc',
    'long_string_field': 'abcdef',
    'scaled_int8_field': 50,
}

class FieldTestObject(Object):
    readonly_field = Field(api_name='api_readonly_field',
                           field_name='field_readonly_field',
                           readonly=True)

    bool_field = BoolField()

    float_field = FloatField()
    int8_field = Int8Field()
    int16_field = Int16Field()
    small_int8_field = Int8Field(max_value=12)
    scaled_int8_field = ScaledInt8Field(max_value=100)

    class SillyEnum(enum.Enum):
        One = 1
        Two = 2
        Three = 3

    enum_field = EnumField(SillyEnum)

    string_field = StringField(max_length=10)
    short_string_field = StringField(max_length=3)
    long_string_field = StringField(min_length=2)

    def update_field_value(self, field, value):
        self.update_data_values([{'success': {join('/', self._path, field.api_name): value}}])


def make_obj():
    return FieldTestObject(None, OBJECT_JSON, 'obj')

@nottest
def test_valid_field_values(name, values):
    obj = make_obj()
    assert getattr(obj, name) == OBJECT_JSON[name]

    def check_valid_field_value(value):
        setattr(obj, name, value)
        assert getattr(obj, name) == value

    for value in values:
        yield check_valid_field_value, value

@nottest
def test_invalid_field_data(name, values, exception):
    obj = make_obj()

    @raises(exception)
    def check_invalid_data(name, value):
        setattr(obj, name, value)

    for value in values:
        yield check_invalid_data, value

@nottest
def test_invalid_field_values(name, values):
    test_invalid_field_data(name, values, ValueError)

@nottest
def test_invalid_field_types(name, values):
    test_invalid_field_data(name, values, TypeError)

#
# Field
#

class TestField(unittest.TestCase):
    def setUp(self):
        self.obj = make_obj()

    def test_explicit_field_name(self):
        # This is testing the metaclass's __new__ method.
        self.assertEqual(self.obj.__class__.readonly_field.field_name, 'field_readonly_field')

    def test_explicit_api_name(self):
        # This is testing the metaclass's __new__ method.
        self.assertEqual(self.obj.__class__.readonly_field.api_name, 'api_readonly_field')

    def test_implicit_field_name(self):
        # This is testing the metaclass's __new__ method.
        self.assertEqual(self.obj.__class__.bool_field.field_name, 'bool_field')

    def test_implicit_api_name(self):
        # This is testing the metaclass's __new__ method.
        self.assertEqual(self.obj.__class__.bool_field.api_name, 'bool_field')

    def test_readonly_field(self):
        with self.assertRaises(TypeError):
            self.obj.readonly_field = 'bar'

#
# BoolField
#

def test_bool_field_valid_values():
    test_valid_field_values('bool_field', [True, False])

def test_bool_field_invalid_types():
    test_invalid_field_types('bool_field', ['a', None])

#
# EnumField
#

def test_enum_field_valid_values():
    test_valid_field_values('enum_field', list(FieldTestObject.SillyEnum))

def test_enum_field_invalid_values():
    test_invalid_field_values('enum_field', [3.4, False, None, 'abc'])

@raises(TypeError)
def test_enum_field_nonenum_class():
    class BadEnum:
        Foo = 3
    EnumField(BadEnum)

#
# FloatField
#

def test_float_field_valid_value():
    test_valid_field_values('float_field', [3.4, 3.0, 3, 10])

def test_float_field_invalid_value():
    test_invalid_field_values('float_field', [-1.0])

def test_float_field_invalid_types():
    test_invalid_field_types('float_field', ['a', None, False])

#
# IntegerField, et al.
#

def test_integer_invalid_types():
    test_invalid_field_types('int8_field', ['a', None, False])

def test_int8_valid_values():
    test_valid_field_values('int8_field', [0, 100, 255, 1000])

def test_int8_invalid_values_low():
    test_invalid_field_values('int8_field', [-1, 256])

def test_int16_valid_values():
    test_valid_field_values('int16_field', [0, 1000, 65535, 1000000])

def test_int16_invalid_values_low():
    test_invalid_field_values('int16_field', [-1, 65536])

def test_small_int8_valid_values():
    test_valid_field_values('int8_field', [0, 4, 8])

def test_small_int8_invalid_values_low():
    test_invalid_field_values('int8_field', [-1, 9])

def test_scaled_int8_field_valid_values():
    obj = make_obj()

    def check_scaled_value(scaled_value, unscaled_value):
        obj.scaled_int8_field = scaled_value
        assert obj._data['scaled_int8_field'] == unscaled_value, '{} != {}'.format(obj._data['scaled_int8_field'],
                unscaled_value)

    for scaled, unscaled in [(0.5, 50), (0.25, 25), (0.0, 0), (1.0, 100)]:
        yield check_scaled_value, scaled, unscaled

#
# StringField
#

@raises(ValueError)
def test_string_field_min_length():
    StringField(min_length=-1)

@raises(ValueError)
def test_string_field_max_length():
    StringField(min_length=1, max_length=0)

def test_string_field_valid_values():
    test_valid_field_values('string_field', ['', 'abc', 'abcdefghij'])

def test_string_field_invalid_types():
    test_invalid_field_types('string_field', [3, 3.4, None, False])

def test_short_string_field_invalid_values():
    test_invalid_field_values('long_string_field', ['a'])

def test_long_string_field_invalid_values():
    test_invalid_field_values('short_string_field', ['abcd'])
